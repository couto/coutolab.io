---
author: 'Luís Couto'
title: 'Curriculum Vitae'
date: 2018-11-26T19:01:50Z
hidden: true
---

## Experience

Dec 2017 - Present
: [YLD.io][]
  **Senior JavaScript Developer**

Oct 2015- Nov 2017
: [Mindera][]
  **Senior JavaScript Developer**

  - Front-end development with [angularjs][] and [requirejs][] for mobile and desktop applications
  - Migration of [angularjs][] applications to [React][], [Redux][],
    [styled-components][] and [Webpack][] with special care to organize the UI
    components in [storybook][]
  - Server-side rendered React applications with [expressjs][]
  - Application organization with micro-frontends
  - Development of conventions and project structure standard with library sharing and package organization with [lerna][]

Jun 2013- Oct 2015
: [Musikki][]
  **JavaScript Developer**

  - Front-end development with [jQuery][], [Sass][] and [requirejs][]
  - Use of [Backbone.js] to improve code organization
  - [React][] development with [Flummox][]
  - REST API development with [Node.js][], [hapi][], [sequelize][] and [PostgreSQL] with a microservice architecture in mind.
  - Servers provisioning with [Ansible]

Jun 2011 - May 2013
: [Triworks][]
  **Front-End Developer**

  - Single Page Applications with in-house framework, [MooTools][], [Sass][] and [requirejs][]
  - UI and animation implementation with special attention to details
  - Organization of a components style-guide and UI library
  
Sep 2010 - Jun 2011
: [labs.sapo/ua][]
  **Front-End Developer**

  - Widget based dashboard with application-level isolation
  - Single-Page applications with [Backbone.js][]

[angularjs]: https://angularjs.org/
[requirejs]: https://requirejs.org/
[react]: https://reactjs.org/
[redux]: https://redux.js.org/
[styled-components]: https://www.styled-components.com/
[webpack]: https://webpack.js.org
[storybook]: https://storybook.js.org/
[expressjs]: https://expressjs.com/
[lerna]: https://lernajs.io/
[jquery]: https://jquery.com/
[backbone.js]: http://backbonejs.org/
[sass]: http://sass-lang.com/
[flummox]: https://acdlite.github.io/flummox/
[node.js]: https://nodejs.org
[hapi]: https://hapijs.com/
[sequelize]: http://docs.sequelizejs.com/
[Ansible]: https://www.ansible.com/
[PostgreSQL]: https://www.postgresql.org/
[MooTools]: https://mootools.net/

[YLD.io]: https://www.yld.io
[Mindera]: https://mindera.com
[Musikki]: https://www.musikki.com
[Triworks]: https://www.youtube.com/watch?v=Kw04ckfO-yA
[labs.sapo/ua]: http://labs.sapo.pt/

## Technical Experience

Open Source
: Over the years I contributed to Open Source projects, either by fixing small
  bugs, [refactors][] or creating my own projects:

  - [babel-loader][]: A [Webpack][] loader that allows people to use [Babel][]
    in their projects. 

    While I'm the original author, the project is now maintained by the awesome
    [Babel Team][] **Thank you!**

  - [groundskeeper][]:  A small utility to remove `console` statements from
    code, made at a time when minifiers didn't support that feature.

    Featured in [JavaScript Weekly][] in 2013.  
    Every once in a while a new tool based on groundskeeper [springs up][].

Programming Languages
: [JavaScript][] The language of choice for work. I've been working with
  JavaScript since 2010.

: [Elm][] I've been doing experiments with Elm over the years. It's a small
  pure-functional language, with great potential.  
  Some projects I've done:

    - [Kanban Board][]
    - [Advent of Code 2017][].

[refactors]: https://github.com/ticketmaster/storybook-styled-components/pull/1
[Babel]: https://babeljs.io/
[Babel Team]: https://github.com/orgs/babel/teams/webpack/members
[groundskeeper]: https://github.com/Couto/groundskeeper
[springs up]: https://www.npmjs.com/search?q=groundskeeper
[javascript weekly]: https://javascriptweekly.com/
[babel-loader]: https://github.com/babel/babel-loader
[elm]: https://elm-lang.org/
[kanban board]: https://github.com/Couto/tasks
[advent of code 2017]: https://gitlab.com/couto/advent-of-code

## Education

2010
: **Interface Design**, [Bauhaus-Universität Weimar][]

2007-2010
: **BSc, New Technologies of Communication**, [University of Aveiro][], 2007-2010

2004-2007
: **Level III Multimedia**, [Escola Profissional Mariana Seixas][], 2004-2007

[bauhaus-universität weimar]: https://www.uni-weimar.de/en/university/start/
[university of aveiro]: https://www.ua.pt/
[escola profissional mariana seixas]: http://www.epms.pt/


