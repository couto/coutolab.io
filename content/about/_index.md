---
title: 'About me'
date: 2018-11-26T19:01:50Z
hidden: true
menu: 'main'
---

I'm Luís Couto. I'm a software developer from Portugal.

You can find my work over [Github][] or on [GitLab][]. 
If you're interested on my random thoughts you can find them on [Mastodon][], or
[Twitter][].

If you're looking for information about previous professional jobs, you can find
it on [LinkedIn][] or on my [Curriculum Vitae][] page.

My [PGP key][] lives at [keybase][], [MIT keyserver][] or directly on this blog.

You can reach me at [hello@luiscouto.pt][] or chat directly at
[@couto:matrix.org][].

[Github]: https://github.com/Couto
[GitLab]: https://gitlab.io/Couto
[LinkedIn]: https://www.linkedin.com/in/lcouto87/
[Twitter]: https://twitter.com/coutoantisocial
[keybase]: https://keybase.io/couto
[MIT keyserver]: https://pgp.mit.edu/pks/lookup?op=get&search=0x7C625B90A6816DA3
[PGP key]: /pgp/6615D57E7D207B0C537CADDF7C625B90A6816DA3.asc
[Curriculum Vitae]: {{< relref "curriculum-vitae.md" >}}
[Mastodon]: https://mastodon.technology/@couto
[hello@luiscouto.pt]: mailto://hello+fromblog@luiscouto.pt
[@couto:matrix.org]: https://matrix.to/#/@couto:matrix.org
