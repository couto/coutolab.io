---
title: "An alternative Universe for Frontend Development"
date: 2019-03-08T22:04:52Z
draft: false
---

> **Disclaimer:** This text is part of an introductory talk about Elm.
> If you're looking for an in-depth guide to Elm please check
[guide.elm-lang.org][].

Those who pay some degree of attention to theories about space and the
universe, should know that there is some discussion about the possibility of
existing more than one universe, personally I find this super exciting,
specially because of all the possibilities in the realm of science-fiction.

Are theses universes bound by the same laws of physics? Or were these laws
defined on the moment of the Big Bang of each universe?

Without being able to observe these universes, we might never find out.

Fortunately for my train of thought, "universe" might also mean: "the aggregate
of all the objects, attributes, and relations assumed or implied in a given
discussion."

Which brings us to the point of this discussion, the existing universes of
Front-End Development.

Of all the universes on the Front-End ecosystem, the biggest one at the moment
is [React][] with all the related tooling around it, but we know of others like
[Vue.js][] that are gaining traction. One of these smaller universes, is the
[Elm][] universe.

Elm is a pure functional language, maintained by [Evan Czaplicki][], it was
originally created as part of their thesis on [Concurrent FRP for Functional
GUIs][], although it has evolved since then.

## Notable Features

At the time Elm took different approaches to solve the "How to build Web Apps"
problem, some approaches heavily inspired solutions in other universes, like
[Redux][], other features suffered from [convergent evolution][], like the
virtual-dom.

### ML inspired language

Elm as a language is quite lean and small, there's just not much to learn
(according to some this is debatable), and that's part of the beauty of it,
also, everything is pure and immutable.

A simple snippet that sums 2 to every item in a list can be written as:

```
sum n = n + 2

List.map sum [1,2,3,4]

--- [3,4,5,6,7]
```

In the above snippet, we created a function `sum` that takes an argument `n` and
adds `2` to whatever `n` is.

We then used a function from the [core][] package to manipulate lists. This
function accepts a function and a list, and then for each item on the list, it
calls the received function with that list. In the end it creates a new list
with the results.

This is not the most magnificent code ever, but it's concise and shows how small
the language is too.

#### Immutability

Also, if you notice, I said that it returns a *new* list. Everything in Elm is
immutable. This is important to know, and is also a feature that makes the code
simpler and less prone to bugs.

Let's take a [Record][] per example:

```
person = 
    { firstName = "John"
    , lastName = "Doe"
    }
```

If I want to change the name of that person to "Tyler Durden", I do it like:

```
namedPerson = 
    { person | firstName = "Tyler", lastName = "Durden" }
```

This is not changing the `person` record, that's impossible, this is returning a
new record for Tyler Durden.

If you never worked with records before, the easiest way to think about them is
to compare then with JSON objects, don't confuse with JavaScript objects. JSON
objects like Records, are also immutable if you think about them.

#### Purely Functional Language

I said that Elm is a purely functional language, and by that I mean that it
only admits a functional programming style, unlike JavaScript where you can
write code in a more functional or object-oriented style.

In order words, you won't be able to do side-effects, nor even something that
looks simple like generating a random number.

```
const n = Math.random() // 0.3
```

To do the same in Elm is… a lot more [complicated][].


### Type System

Apart from being a different language, this is one of the major differences from
Elm when compared with JavaScript solutions. Elm applications have *no runtime
errors* this is possible because of a powerful type system and a pretty good
compiler. (Disclaimer: It's possible to have runtime errors if developers really
go to extreme situations)

When we write code like the above:

```
sum n = n + 2

List.map sum [1,2,3,4]
```

The compiler knows that `n` can only be a number, because the `(+)` is a
function that accepts numbers, and since Elm is a purely functional language it
becomes trivial to track this value wherever it's used.

While not necessary, we can also define the types explicitly:

```
sum : Int -> Int -> Int
sum n = n + 2

```

What does the above means?

```
sum : Int -> Int -> Int
^     ^      ^      ^
|     |      |      Return Value
|     |      Second Argument
|     First Argument
Name of the function
```

We can give it a `string` and see what happens:

```
sum : Int -> Int -> Int
sum n = n + 2

sum "Hello"
```

The result with be:

```bash
-- TYPE MISMATCH --------------------------------------------- repl-temp-000.elm

The argument to function `sum` is causing a mismatch.

3|   sum "Hello"
         ^^^^^^^
Function `sum` is expecting the argument to be:

    number

But it is:

    String

```

This type system brings a lot of advantages, from reusable code to easy
refactoring, in fact, refactoring becomes really easy and safe with Elm, since
the compiler is able to track down all the places that need changes.

A fundamental advantage of the type system is that allows us to not forget to
handle application states, e.g.:

```
type Msg 
    = Increment
    | Decrement


update : Msg -> Model -> Model
update msg model =
  case msg of
    Increment ->
      model + 1
```

Just so you know, `update` is a function that is part of [The Elm
Architecture][] and we will deal with it later.

If you're not familiar with pattern matching, you can think of `case` as a
`switch` on steroids.

In this scenario, we are telling that the `update` function accepts a value of
type `Msg`, and we then handle what happens if that value is of type
`Increment`, but we ignore the possibility of the value being of type
`Decrement`:

```
This `case` does not have branches for all possibilities:

24|>    case msg of
25|>        Increment ->
26|>            { model | count = model.count + 1 }

Missing possibilities include:

    Decrement

I would have to crash if I saw one of those. Add branches for them!

Hint: If you want to write the code for each branch later, use `Debug.todo` as a
placeholder. Read <https://elm-lang.org/0.19.0/missing-patterns> for more
guidance on this workflow.
```

As you can see, the compiler complains about the fact that we are not dealing
with a possible state. This is good, a bit of a pain for the developer, but is a
good thing for our applications, by making sure that we handle all possible
scenarios we make sure that our application never reaches an unexpected state,
helping us avoiding runtime errors.

### Automatic Package Versioning

This is little known feature of Elm that I find delightful! 

Elm doesn't use the [npm registry][] but uses its own [Elm registry] instead,
this registry only accepts Elm packages.

As you've probably noticed, the Elm compiler has an extensive knowledge of our
code, so much that is able to infer the correct semantic versioning of our
packages. In fact, you as a developer, are unable to define your package's
version.

This solves a lot of problems, specially with breaking changes that don't feel
like "major changes", in the JavaScript universe it's pretty common for
maintainers to bump the minor version with breaking changes, this scenario is
impossible in Elm.

With this kind of control, some advantages arise, the compiler can provide diffs
between versions, so that we as developers can realise what changed between
packages.

e.g.:

```
❯ elm diff elm/json 1.0.0 1.1.0
```

Results in:

```
This is a MINOR change.

---- Json.Decode - MINOR ----

    Added:
        oneOrMore : (a -> List a -> value) -> Decoder a -> Decoder value

```

### UI as Elm

Elm is a language to build Web applications, at some point we need to actually
build UI elements.
Unlike React or Vue, which have their own special syntax to build UI elements,
Elm uses Elm itself. There is no concept of "component" in Elm, everything is a
function:

```
import Html exposing (..)


articleView : Html Msg
articleView =
    article [ class "it-has-so-much-class" ]
        [ h1 [] [ text "Hello World" ]
        , p [] [ text "Lorem Ipsum dolor sit amet" ]
        ]
```

Most functions that builds UI have the same Type signature:

```
h1 : List (Attribute msg) -> List (Html msg) -> Html msg
```

in other words:

1. A list of attributes (which are functions themselves).
2. A list of other Html functions.
3. Returns an Html function

We will deal the `msg` later.


We are building UI as functions, therefor we can treat them as… well functions!

```
import Html exposing (..)


articleView : Model -> Html Msg
articleView model =
    article [ class "it-has-so-much-class" ]
        [ h1 [ id model.id ] [ text "Hello World" ]
        , p [] [ text "Lorem Ipsum dolor sit amet" ]
        ]


blog : Html Msg
blog =
    header []
        [ articleView { id = 1 }
        , footer [] []
        ]
```

This is just a contrived example, but we can see how we can reuse UI elements
across our codebase.

### The Elm Architecture

This is the fun part of Elm, I describe this architecture as rigid set of steps
that your application will perform every time something happens. This
information is unidirectional and allows our applications to be quite
predictable.

If this sounds familiar, that's because is the current approach being used in
Redux and Vuex applications. [Redux][] itself doesn't even deny that Elm was a
major source of inspiration.

This architecture is based on `Model`, `update` and `view`.

When the user interacts with our `view`, this function will produce values of
type `Msg`. These values are then passed to our `update` function, like the one
we saw above. This update function will then create a new `Model` with values
changed according to the value received.

Our `view` function is then called with our new `model` and update the UI with
the result.

![The Elm Architecture diagram](https://freecontent.manning.com/wp-content/uploads/handling-user-input_01.png)  
*Image from https://freecontent.manning.com/handling-user-input-with-the-elm-architecture/*

No matter how much I talk about the architecture, the best way of understanding
it to watch it in action with a small demo.



[guide.elm-lang.org]: https://guide.elm-lang.org
[React]: https://reactjs.org/
[Vue.js]: https://vuejs.org/
[Elm]: https://elm-lang.org/
[Evan Czaplicki]: https://twitter.com/czaplic
[Concurrent FRP for Functional GUIs]: https://elm-lang.org/assets/papers/concurrent-frp.pdf
[Redux]: https://redux.js.org/introduction/prior-art#elm
[convergent evolution]: https://www.youtube.com/watch?v=jl1tGiUiTtI 
[core]: https://package.elm-lang.org/packages/elm/core/1.0.2
[Record]: https://elm-lang.org/docs/records
[complicated]: https://guide.elm-lang.org/effects/random.html
[npm registry]: https://npmjs.org/
[Elm registry]: https://package.elm-lang.org
