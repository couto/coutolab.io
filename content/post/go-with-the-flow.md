+++
date = "2017-06-17T20:16:12+00:00"
title = "the cliche that hurted my feelings"
draft = true
+++

See: https://en.wikipedia.org/wiki/Dramatic_structure

1. Exposition
2. Rising Action
3. Climax
4. Falling Action
5. Dénouement

See: https://en.wikipedia.org/wiki/Three-act_structure

You work on that awesome project for some months now, as a solo developer, and your manager tells you the client loved the product that you helped developed and that they plan to increase the team size.

Great news! You get to have help from new developers and it allows a faster and more consistent delivery of features.

