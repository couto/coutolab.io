# couto.gitlab.io

[![build status](https://gitlab.com/Couto/couto.gitlab.io/badges/master/build.svg)](https://gitlab.com/Couto/couto.gitlab.io/commits/master)

## Stack

  * [Hugo - A fast and modern static engine](https://gohugo.io/)
  * [Write-Good -  Naive linter for English prose ](https://github.com/btford/write-good)

## License

All posts in this project are licensed under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/)
[![Creative Commons License](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)

The blog itself is licensed under a [MIT License](https://couto.mit-license.org/),
unless stated otherwise.
